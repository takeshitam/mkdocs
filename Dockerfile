FROM registry.gitlab.com/takeshitam/openjdk

LABEL maintainer takeshitam <mst.take@gmail.com>

RUN yum -y install python36 && yum clean all \
    && ln -s /usr/bin/python36 /usr/bin/python3 \
    && python3 -m ensurepip --upgrade \
    && pip3 install --upgrade pip

WORKDIR /opt/plantuml
RUN yum -y install graphviz
RUN curl -L http://sourceforge.net/projects/plantuml/files/plantuml.jar/download -o plantuml.jar
RUN curl -L https://gitlab.com/takeshitam/pte/-/jobs/artifacts/0.1/download?job=compile -o /usr/local/bin/pte
COPY plantuml /usr/local/bin/plantuml
RUN chmod a+x /usr/local/bin/plantuml /usr/local/bin/pte

WORKDIR /docs
RUN pip3 install mkdocs mkdocs-material plantuml-markdown fontawesome_markdown 

